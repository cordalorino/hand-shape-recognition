package main;

import feature.Feature;
import feature.FingerFeature;
import imageprocessing.FeatureExtractor;
import imageprocessing.ImageProcessingHelper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import org.apache.commons.io.FilenameUtils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.videoio.VideoCapture;
import utils.Settings;
import utils.Utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class Controller {
    public static final Size BLUR_KERNEL_SIZE = new Size(5, 5);

    @FXML
    private Button btnSnap;
    @FXML
    private Button btnAddUser;
    @FXML
    private Button btnDeleteUser;

    @FXML
    private ListView lstUser;
    @FXML
    private TextFlow txtLog;
    @FXML
    private ScrollPane scrollPane;
    @FXML
    private ImageView imgCamera;
//    @FXML
//    public ImageView imgHand1;
//    @FXML
//    public ImageView imgHand2;

    Settings settings = Settings.getInstance();

    // a timer for acquiring the video stream
    private ScheduledExecutorService timer;
    // the OpenCV object that realizes the video capture
    private VideoCapture capture = new VideoCapture();
    // a flag to change the button behavior
    private boolean cameraActive = false;
    // the id of the camera to be used
    private static int cameraId = 0;

    //Rect roi = new Rect(800, 100, 250, 300);
    Rect roi = new Rect(
            settings.getRoiX(),
            settings.getRoiY(),
            settings.getRoiWidth(),
            settings.getRoiHeigth()
    );

    public static Controller instance;

    private boolean enrollmentState = false;
    private int enrollmentCounter = 0;
    private String enrollmentName;

    private FeatureExtractor[] enrollmentExtractors;
    private int c = 0;


    @FXML
    protected void addUser(ActionEvent event) {

        final TextInputDialog inputDlg = new TextInputDialog("");

        inputDlg.setTitle("Insert Name");
        inputDlg.setContentText("Name");
        inputDlg.setHeaderText(null);

        String name, filepath;
        File file = null;

        do {

            enrollmentName = inputDlg.showAndWait().get();

            filepath = "user/" + enrollmentName + ".json";

            file = new File(filepath);

        } while (file.exists());

        enrollmentState = true;
        enrollmentExtractors = new FeatureExtractor[3];

    }

    @FXML
    protected void deleteUser(ActionEvent event) {

        String selectedUser = lstUser.getSelectionModel().getSelectedItem().toString();

        File userProfile = new File(getProfilePath(selectedUser));

        userProfile.delete();

        initUserList();
    }


    @FXML
    protected void takeSnap(ActionEvent event) {


        if (enrollmentState) {
            enrollmentProcedure();
        } else {
            testingProcedure();
        }
    }


    private void enrollmentProcedure() {
        FeatureExtractor featureExtractor = ImageProcessingHelper.featureExtractor;
//        if (featureExtractor.isFeaturesValid()){
//            publishTolog("Error acquiring sample " + enrollmentCounter,MessageTypes.ERR);
//            return;
//        }
        enrollmentExtractors[enrollmentCounter] = featureExtractor;

        enrollmentCounter++;

        publishTolog("Acquired sample number " + enrollmentCounter, MessageTypes.INFO);

        if (enrollmentCounter == 3) {
            enrollmentCounter = 0;
            enrollmentState = false;

            Feature enrollmentFeature = new Feature(
                    enrollmentExtractors[0].getFingerFeatures(),
                    enrollmentExtractors[1].getFingerFeatures(),
                    enrollmentExtractors[2].getFingerFeatures()
            );

            try {
                enrollmentFeature.saveToFile(getProfilePath(enrollmentName));
                initUserList();
            } catch (IOException e) {
                publishTolog("Exception saving user profile: " + e, MessageTypes.ERR);
                e.printStackTrace();
            }

        }
    }

    private void testingProcedure() {

        String selectedUser = lstUser.getSelectionModel().getSelectedItem().toString();
        double confidence = 0;

        try {

            Feature enrollmentFeature = Feature.readFromFile(getProfilePath(selectedUser));

            FeatureExtractor featureExtractor = ImageProcessingHelper.featureExtractor;

//            if (featureExtractor.isFeaturesValid()){
//                publishTolog("Error acquiring sample ",MessageTypes.ERR);
//                return;
//            }

            FingerFeature[] fingerFeatures = featureExtractor.getFingerFeatures();

            if (fingerFeatures == null) {
                publishTolog("Error acquiring hand data", MessageTypes.ERR);
                return;
            }

            Feature testFeature = new Feature(fingerFeatures);

            confidence = Feature.compareFeatures(enrollmentFeature, testFeature);

            publishTolog("Confidence for user " + selectedUser + ": " + confidence + "%", MessageTypes.RES);

        } catch (IOException e) {
            publishTolog("Error when loading user profile", MessageTypes.ERR);
            System.err.println("Exception loading user profile: " + e);
            e.printStackTrace();
        }
    }


    private String getProfilePath(String name) {
        return "user/" + name + ".json";
    }

    public void setupForm() {

        initUserList();

        instance = this;

    }

    private void initUserList() {
        lstUser.getItems().clear();

        for (File f : new File("user").listFiles()) {

            String name = f.getName();

            String ext = FilenameUtils.getExtension(name);

            if (ext.equals("json")) {
                name = name.substring(0, name.length() - 5);

                lstUser.getItems().add(name);
            }
        }

        if (lstUser.getItems().size() > 0) {
            lstUser.getSelectionModel().select(0);
        }


    }

    public static void publishTolog(String textString, MessageTypes type) {
        Date date = new Date();
        String timestamp = new SimpleDateFormat("HH:mm:ss.SSSZ").format(date);

        Text text = new Text();
        text.setFont(Font.font("Courier New", 14));
        String typeChar = "INFO";
        switch (type) {
            case INFO:
                typeChar = "INFO";
                text.setFill(Color.BLACK);
                break;
            case ERR:
                typeChar = "ERR";
                text.setFill(Color.RED);
                break;
            case RES:
                typeChar = "RES";
                text.setFill(Color.GREEN);
                break;
        }
        textString = String.format("[%s] %s - %s\n", timestamp, typeChar, textString);
        text.setText(textString);
        instance.txtLog.getChildren().add(text);
        try {
            instance.scrollPane.layout();
            instance.scrollPane.setVvalue(1.0f);
        } catch (NullPointerException ignored) {
        }
    }

    public void startCamera() {


        publishTolog("System initializing", MessageTypes.INFO);
        if (!this.cameraActive) {
            // start the video capture
            this.capture.open(cameraId);

            // is the video stream available?

            if (this.capture.isOpened()) {
                this.cameraActive = true;
                publishTolog("Camera opened", MessageTypes.RES);
                // grab a frame every 33 ms (30 frames/sec)
                Runnable frameGrabber = new Runnable() {

                    @Override
                    public void run() {
                        // effectively grab and process a single frame
                        Mat frame = grabFrame();

                        // convert and show the frame
                        //Image imageToShow = Utils.mat2Image(frame);
                        //updateImageView(imgCamera, imageToShow);
                    }
                };

                this.timer = Executors.newSingleThreadScheduledExecutor();

                this.timer.scheduleAtFixedRate(frameGrabber, 0, 33, TimeUnit.MILLISECONDS);

            } else {
                // log the error
                System.err.println("Impossible to open the camera connection...");
            }
        } else {
            // the camera is not active at this point
            this.cameraActive = false;
            // update again the button content
            // this.button.setText("Start Camera");

            // stop the timer
            this.stopAcquisition();
        }
    }


    private Mat grabFrame() {
        // init everything
        Mat frame = new Mat();

        // check if the capture is open
        if (this.capture.isOpened()) {
            try {
                // read the current frame
                this.capture.read(frame);

                // if the frame is not empty, process it
                if (!frame.empty()) {

                    frame = ImageProcessingHelper.processImage(frame, roi);

                    Mat frameClipped = frame.clone();
                    Core.flip(frame, frameClipped, 90);

                    imgCamera.setImage(Utils.mat2Image(frameClipped));
                }


            } catch (Exception e) {
                // log the error
                e.printStackTrace();
                System.err.println("Exception during the image elaboration: " + e);
            }
        }

        return frame;
    }

    private void stopAcquisition() {
        if (this.timer != null && !this.timer.isShutdown()) {
            try {
                // stop the timer
                this.timer.shutdown();
                this.timer.awaitTermination(33, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                // log any exception
                System.err.println("Exception in stopping the frame capture, trying to release the camera now... " + e);
            }
        }

        if (this.capture.isOpened()) {
            // release the camera
            this.capture.release();
        }
    }

    private void updateImageView(ImageView view, Image image) {
        Utils.onFXThread(view.imageProperty(), image);
    }

    protected void setClosed() {
        this.stopAcquisition();
    }


}
