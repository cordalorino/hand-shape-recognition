package main;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.org.apache.regexp.internal.RE;
import feature.Feature;
import feature.FingerFeature;
import imageprocessing.FeatureExtractor;
import imageprocessing.ImageProcessingHelper;
import org.apache.commons.io.FileUtils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.imgcodecs.Imgcodecs;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.*;
import java.util.List;

/**
 * Created by andre on 02/02/2017.
 */
public class Test {

    public static void main(String[] args) {

        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        //generateEnroll();

        //moveValidImages();

        //test();


       // for (double i = 0; i <= 100; i += 5) {
        //    elaborateTests(i);
       // }


    }


    private static void elaborateTests(double threshold) {

        File dbDir = new File("test");


        Result result = new Result();


        for (File f : dbDir.listFiles()) {

            if (f.isDirectory()) continue;

            try {

                Gson gson = new Gson();
                Double[] accArray = gson.fromJson(FileUtils.readFileToString(f, "UTF-8"), Double[].class);

                List<Double> acc = Arrays.asList(accArray);

                double tempAcc = acc.get(0);

                //temp
                acc.set(0, 100.0);

                if (tempAcc >= threshold) {
                    result.truePositive++;
                } else {
                    result.falseNegative++;
                }

                Collections.sort(acc);

                for (int i = 0; i < acc.size() - 12; i++) {

                    if (acc.get(i) >= threshold) {

                        result.falsePositive++;
                    } else {

                        result.trueNegative++;
                    }
                }


            } catch (IOException e) {
                e.printStackTrace();
            }

        }


        double[] out = {result.truePositive, result.falseNegative, result.trueNegative, result.falsePositive};


        File outFile = new File("test/out/" + threshold + "%.json");

        //Gson gson = new GsonBuilder().setPrettyPrinting().create();

        Gson gson = new Gson();

        try {
            String data = gson.toJson(out);
            FileUtils.writeStringToFile(outFile, data, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }


//        File dbDir = new File("test");
//
//        //  |   truePositive    falsePositive   |
//        //  |   trueNegative    falseNegative   |
//
//        int[][] matrix = {{0, 0}, {0, 0}};
//
//
//        Result result = new Result();
//
//        for (File f : dbDir.listFiles()) {
//
//            if (f.isDirectory()) continue;
//
//            try {
//
//                Gson gson = new Gson();
//                double[] acc = gson.fromJson(FileUtils.readFileToString(f, "UTF-8"), double[].class);
//
//                if (acc[0] >= threshold) {
//                    matrix[0][0]++;
//                    result.truePositive++;
//                } else {
//                    matrix[1][1]++;
//                    result.falseNegative++;
//                }
//
//                if (acc[1] >= threshold) {
//                    matrix[0][0]++;
//                    result.truePositive++;
//                } else {
//                    matrix[1][1]++;
//                    result.falseNegative++;
//                }
//
//                if (acc[2] >= threshold) {
//                    matrix[0][1]++;
//                    result.falsePositive++;
//                } else {
//                    matrix[1][0]++;
//                    result.trueNegative++;
//                }
//
//                if (acc[3] >= threshold) {
//                    matrix[0][1]++;
//                    result.falsePositive++;
//                } else {
//                    matrix[1][0]++;
//                    result.trueNegative++;
//                }
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//        }
//
//        File outFile = new File("test/out/" + threshold + "%.json");
//
//        Gson gson = new GsonBuilder().setPrettyPrinting().create();
//
//        try {
//            String data = gson.toJson(result);
//            FileUtils.writeStringToFile(outFile, data, "UTF-8");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }


    static class Result {
        public int truePositive = 0;
        public int falseNegative = 0;

        public int trueNegative = 0;
        public int falsePositive = 0;

        public int getTruePositive() {
            return truePositive;
        }

        public void setTruePositive(int truePositive) {
            this.truePositive = truePositive;
        }

        public int getFalseNegative() {
            return falseNegative;
        }

        public void setFalseNegative(int falseNegative) {
            this.falseNegative = falseNegative;
        }

        public int getTrueNegative() {
            return trueNegative;
        }

        public void setTrueNegative(int trueNegative) {
            this.trueNegative = trueNegative;
        }

        public int getFalsePositive() {
            return falsePositive;
        }

        public void setFalsePositive(int falsePositive) {
            this.falsePositive = falsePositive;
        }
    }

    private static void test() {


        File dbDir = new File("user");


        LinkedList<Double> acc;

        for (File f : dbDir.listFiles()) {

            if (f.isDirectory()) continue;

            acc = new LinkedList<>();

            String id = f.getName().replace(".json", "");

            try {

                String path = String.format("dataset/%s-%s.jpg", id, 4);

                double tempAcc = getAccuracy(f.getPath(), path);

                acc.add(tempAcc);

            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

            for (File f1 : dbDir.listFiles()) {

                try {
                    if (f1.isDirectory()) continue;

                    String id1 = f1.getName().replace(".json", "");

                    if (id1.equals(id)) continue;

                    String path = String.format("dataset/%s-%s.jpg", id1, 1);

                    double tempAcc = getAccuracy(f.getPath(), path);

                    acc.add(tempAcc);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


            Gson gson = new Gson();

            String accJsonString = gson.toJson(acc);

            try {
                FileUtils.writeStringToFile(new File("test/" + id + ".json"), accJsonString, "UTF-8");
            } catch (IOException e) {
                e.printStackTrace();
            }


        }


//        File dbDir = new File("user");
//
//        int i = 0;
//
//        StringBuilder stringBuilder = new StringBuilder();
//
//        for (File f : dbDir.listFiles()) {
//
//            if (f.isDirectory()) continue;
//
//            String id = f.getName().replace(".json", "");
//
//            double acc[] = new double[4];
//
//
//            try {
//
//                String path = String.format("dataset/%s-%s.jpg", id, 4);
//
//                acc[0] = getAccuracy(f.getPath(), path);
//
//                path = String.format("dataset/%s-%s.jpg", id, 5);
//
//                acc[1] = getAccuracy(f.getPath(), path);
//
//                path = getRandomImage(id);
//
//                acc[2] = getAccuracy(f.getPath(), path);
//
//                path = getRandomImage(id);
//
//                acc[3] = getAccuracy(f.getPath(), path);
//
//                Gson gson = new Gson();
//
//                String accJsonString = gson.toJson(acc);
//
//                FileUtils.writeStringToFile(new File("test/" + id + "_5.json"), accJsonString, "UTF-8");
//
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//
//            //System.out.println(i + " -> " + acc[0] + " -> " + acc[1]);
//
//            //   stringBuilder.append(i + " -> " + acc[0] + " -> " + acc[1]);
//
//
//        }


        // System.out.println(stringBuilder.toString());

    }


    private static String getRandomImage(String exclude) {


        Random random = new Random();

        File image;
        File dir = new File("dataset");

        File[] files = dir.listFiles();

        int nFiles = dir.listFiles().length;

        String filepath = "";

        do {

            int randInt = random.nextInt(nFiles);

            image = files[randInt];

        } while (image.getName().startsWith(exclude));


        return image.getPath();
    }

    private static double getAccuracy(String profilepath, String imagePath) throws IOException {

        Feature enrollmentFeature = Feature.readFromFile(profilepath);

        Mat frame = Imgcodecs.imread(imagePath);

        Core.flip(frame, frame, 90);

        Rect roi = new Rect(new Point(0, 0), frame.size());

        ImageProcessingHelper.processImage(frame, roi);

        FeatureExtractor featureExtractor = ImageProcessingHelper.featureExtractor;

        FingerFeature[] fingerFeatures = featureExtractor.getFingerFeatures();

        Feature testFeature = new Feature(fingerFeatures);

        return Feature.compareFeatures(enrollmentFeature, testFeature);

    }


    private static void moveValidImages() {

        File dbDir = new File("user");

        for (File f : dbDir.listFiles()) {

            if (f.isDirectory()) continue;

            String id = f.getName().replace(".json", "");

            for (int i = 1; i <= 5; i++) {

                try {

                    String from = String.format("dataset/%s-%s.jpg", id, i);
                    String to = String.format("dataset/valid/%s-%s.jpg", id, i);

                    File imageFrom = new File(from);
                    File imageTo = new File(to);

                    FileUtils.copyFile(imageFrom, imageTo);

                    System.out.println(from + "  -->  " + to);

                } catch (Exception e) {
                }

            }
        }
    }


    private static void generateEnroll() {
        File dbDir = new File("dataset");

        String id = "", newId;

        for (File f : dbDir.listFiles()) {

            if (f.isDirectory()) continue;

            newId = getId(f.getName());

            if (newId.equals(id)) continue;

            id = newId;

            try {
                enroll(id);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    private static String getId(String filename) {

        int pos = filename.lastIndexOf("-");

        return filename.substring(0, pos);
    }

    private static void enroll(String id) throws IOException {

        Mat frame;

        FeatureExtractor[] enrollmentExtractors = new FeatureExtractor[3];

        for (int i = 0; i < 3; i++) {

            String path = String.format("dataset/%s-%s.jpg", id, i + 1);

            frame = Imgcodecs.imread(path);

            Core.flip(frame, frame, 90);

            frame = ImageProcessingHelper.processImage(frame, new Rect(new Point(0, 0), frame.size()));

            Imgcodecs.imwrite(String.format("dataset/out/%s-%s.jpg", id, i + 1), frame);

            enrollmentExtractors[i] = ImageProcessingHelper.featureExtractor;

        }

        Feature enrollmentFeature = new Feature(
                enrollmentExtractors[0].getFingerFeatures(),
                enrollmentExtractors[1].getFingerFeatures(),
                enrollmentExtractors[2].getFingerFeatures()
        );

        enrollmentFeature.saveToFile("user/" + id + ".json");


    }

}
