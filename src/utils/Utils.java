package utils;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.LinkedList;
import java.util.List;

import org.opencv.core.*;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import org.opencv.imgproc.Imgproc;

import static org.opencv.imgproc.Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C;
import static org.opencv.imgproc.Imgproc.THRESH_BINARY;


public final class Utils {

    public static final int THRESHOLD = 0;
    public static final int MAXVAL = 255;
    public static final int THRESHOLD_TYPE = 9;

    public static void findContours(Mat original, List<MatOfPoint> contours) {
        Mat hierarchy = new Mat();
        Imgproc.findContours(original, contours, hierarchy, Imgproc.RETR_TREE,
                Imgproc.CHAIN_APPROX_SIMPLE, new Point(0, 0));
    }

    public static Mat selectThreshold(Mat original) {
        Mat dest = original.clone();
        Imgproc.threshold(original, dest, THRESHOLD, MAXVAL, 9);
        //Imgproc.adaptiveThreshold(dest, dest,255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 11,2);
        return dest;
    }

    public static Mat selectRoiOfImage(Mat original, Rect roi) {
        return original.submat(roi);
    }

    public static Mat blurImage(Mat original, Size kernelSize) {
        Mat dest = original.clone();
        Imgproc.blur(original, dest, kernelSize);
        return dest;
    }

    public static Image mat2Image(Mat frame) {
        try {
            return SwingFXUtils.toFXImage(matToBufferedImage(frame), null);
        } catch (Exception e) {
            System.err.println("Cannot convert the Mat obejct: " + e);
            return null;
        }
    }

    public static <T> void onFXThread(final ObjectProperty<T> property, final T value) {
        Platform.runLater(() -> {
            property.set(value);
        });
    }

    private static BufferedImage matToBufferedImage(Mat original) {
        // init
        BufferedImage image = null;
        int width = original.width(), height = original.height(), channels = original.channels();
        byte[] sourcePixels = new byte[width * height * channels];
        original.get(0, 0, sourcePixels);

        if (original.channels() > 1) {
            image = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
        } else {
            image = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
        }
        final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(sourcePixels, 0, targetPixels, 0, sourcePixels.length);

        return image;
    }

    public static void drawContours(Mat image, LinkedList<MatOfPoint> contours) {
        Scalar color = new Scalar(0, 255, 0);
        Imgproc.drawContours(image, contours, -1, color);

    }
}
