package utils;

import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import org.opencv.core.Point;
import org.opencv.core.Rect;

import java.io.File;
import java.io.IOException;

public class Settings {

    private int roiX = 800;
    private int roiY = 100;

    private int roiWidth = 250;
    private int roiHeigth = 300;


    private static void setInstance(Settings instance) {
        Settings.instance = instance;
    }

    private static Settings instance = null;

    protected Settings() {
    }


    //now is writing
    private static void readSettings(Settings settings) {

        try {
            //String jsonString = new Gson().toJson(settings);
            // FileUtils.writeStringToFile(new File("settings.json"), jsonString, "UTF-8");

            String jsonString = FileUtils.readFileToString(new File("settings.json"), "UTF-8");
            settings.setInstance(new Gson().fromJson(jsonString, Settings.class));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Settings getInstance() {
        if (instance == null) {
            instance = new Settings();

            readSettings(instance);
        }

        return instance;
    }

    public int getRoiX() {
        return roiX;
    }

    public int getRoiY() {
        return roiY;
    }

    public int getRoiWidth() {
        return roiWidth;
    }

    public int getRoiHeigth() {
        return roiHeigth;
    }


    public void setRoiX(int roiX) {
        this.roiX = roiX;
    }

    public void setRoiY(int roiY) {
        this.roiY = roiY;
    }

    public void setRoiWidth(int roiWidth) {
        this.roiWidth = roiWidth;
    }

    public void setRoiHeigth(int roiHeigth) {
        this.roiHeigth = roiHeigth;
    }

}
