package utils;

public class MathUtils {

    public static double innerAngle(double startX, double startY,  double endX, double endY,double centerX, double centerY){


        double distStartDefect = Math.sqrt((Math.pow(startX-centerX,2)+Math.pow(startY-centerY,2)));
        double distStartEnd = Math.sqrt((Math.pow(startX-endX,2)+Math.pow(startY-endY,2)));
        double distEndDefect = Math.sqrt((Math.pow(endX-centerX,2)+Math.pow(endY-centerY,2)));

        double temp = Math.pow(distStartDefect,2) + Math.pow(distEndDefect,2) - Math.pow(distStartEnd,2);
        temp = temp/(2*distStartDefect*distEndDefect);
        double angle = Math.acos(temp);
        angle = (angle*180/Math.PI);
        return angle;

    }
}
