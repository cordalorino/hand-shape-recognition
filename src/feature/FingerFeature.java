package feature;

public class FingerFeature {
    private double fingerMiddleWidth;
    private double fingerBottomWidth;
    private double fingerHeight;

    enum FingerOrderedFeatures{
        BOTTOM_WIDTH,MIDDLE_WIDTH, HEIGHT;
    }
    public FingerFeature() {
    }

    public FingerFeature(double fingerMiddleWidth, double fingerBottomWidth, double fingerHeight) {
        this.fingerMiddleWidth = fingerMiddleWidth;
        this.fingerBottomWidth = fingerBottomWidth;
        this.fingerHeight = fingerHeight;
    }

    public double getFingerMiddleWidth() {
        return fingerMiddleWidth;
    }

    public void setFingerMiddleWidth(double fingerMiddleWidth) {
        this.fingerMiddleWidth = fingerMiddleWidth;
    }

    public double getFingerBottomWidth() {
        return fingerBottomWidth;
    }

    public void setFingerBottomWidth(double fingerBottomWidth) {
        this.fingerBottomWidth = fingerBottomWidth;
    }

    public double getFingerHeight() {
        return fingerHeight;
    }

    public void setFingerHeight(double fingerHeight) {
        this.fingerHeight = fingerHeight;
    }

    public int getCount(){
        return FingerOrderedFeatures.values().length;
    }
    public double getFeature(int i){
        switch (i){
            case 0:{
                return fingerBottomWidth;
            }
            case 1:{
                return fingerMiddleWidth;
            }
            case 2:{
                return fingerHeight;
            }
        }
        return -1;
    }
}
