package feature;

import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import java.io.File;
import java.io.IOException;

public class Feature {

    RealMatrix featureMatrix;
    RealMatrix stdDevMatrix;
    public static double delta = 0.1;

    public Feature(double[][] featureMatrix, double[][] stdDevMatrix) {
        this.featureMatrix = MatrixUtils.createRealMatrix(featureMatrix);
        this.stdDevMatrix = MatrixUtils.createRealMatrix(stdDevMatrix);
    }

    public Feature(FingerFeature[] fingerFeatures1, FingerFeature[] fingerFeatures2, FingerFeature[] fingerFeatures3) {
        double[][] tempMatrix1 = generateFeatureMatrix(fingerFeatures1);
        double[][] tempMatrix2 = generateFeatureMatrix(fingerFeatures2);
        double[][] tempMatrix3 = generateFeatureMatrix(fingerFeatures3);

        RealMatrix featureMatrix1 = MatrixUtils.createRealMatrix(tempMatrix1);
        RealMatrix featureMatrix2 = MatrixUtils.createRealMatrix(tempMatrix2);
        RealMatrix featureMatrix3 = MatrixUtils.createRealMatrix(tempMatrix3);

        int matrixSize = featureMatrix1.getRowDimension();
        featureMatrix = MatrixUtils.createRealMatrix(matrixSize, matrixSize);
        stdDevMatrix = MatrixUtils.createRealMatrix(matrixSize, matrixSize);

        DescriptiveStatistics statistics;

        for (int i = 0; i < matrixSize; i++) {
            for (int j = i; j < matrixSize; j++) {
                statistics = new DescriptiveStatistics();
                statistics.addValue(featureMatrix1.getEntry(i, j));
                statistics.addValue(featureMatrix2.getEntry(i, j));
                statistics.addValue(featureMatrix3.getEntry(i, j));
                featureMatrix.setEntry(i, j, statistics.getMean());
                stdDevMatrix.setEntry(i, j, statistics.getStandardDeviation());
                System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
                System.out.println(featureMatrix1.getEntry(i, j));
                System.out.println(featureMatrix2.getEntry(i, j));
                System.out.println(featureMatrix3.getEntry(i, j));
                System.out.println(featureMatrix.getEntry(i, j));
                System.out.println(stdDevMatrix.getEntry(i, j));
            }
        }


    }

    public static double compareFeatures(Feature enrollment, Feature test) {
        RealMatrix diffFeatureMatrix = enrollment.featureMatrix.subtract(test.featureMatrix);

        int matrixSize = diffFeatureMatrix.getRowDimension();
        int validFeatures = 0;
        double numberOfFeatures = (1 + matrixSize) * matrixSize / 2;
        numberOfFeatures -= matrixSize;

        double minStdDev = 999;

//        for (int i = 0; i < matrixSize; i++) {
//            for (int j = i+1; j < matrixSize; j++) {
//                double tempStdDev = enrollment.stdDevMatrix.getEntry(i, j);
//                if(tempStdDev < minStdDev && tempStdDev != 0){
//                    minStdDev = tempStdDev;
//                }
//            }
//        }

        for (int i = 0; i < matrixSize; i++) {
            for (int j = i+1; j < matrixSize; j++) {
                double tempStdDev = enrollment.stdDevMatrix.getEntry(i, j);
                double entry = Math.abs(diffFeatureMatrix.getEntry(i, j));
                if (tempStdDev + delta >= entry ) {
                    validFeatures++;
                }
            }
        }

        return 100 * validFeatures / numberOfFeatures;
    }

    public Feature(FingerFeature[] fingerFeatures) {

        double[][] tempMatrix = generateFeatureMatrix(fingerFeatures);

        featureMatrix = MatrixUtils.createRealMatrix(tempMatrix);

    }

    private double[][] generateFeatureMatrix(FingerFeature[] fingerFeatures) {

        int matrixSize = fingerFeatures.length * fingerFeatures[0].getCount();
        double[][] tempMatrix = new double[matrixSize][matrixSize];

        double[] featureArray = new double[matrixSize];

        int arrayIndex = 0;

        for (int i = 0; i < fingerFeatures.length; i++) {

            for (int j = 0; j < fingerFeatures[i].getCount(); j++) {

                featureArray[arrayIndex] = fingerFeatures[i].getFeature(j);
                arrayIndex++;
            }
        }

        for (int i = 0; i < matrixSize; i++) {

            for (int j = 0; j < matrixSize; j++) {

                if (i > j) {
                    tempMatrix[i][j] = 0;
                } else {
                    tempMatrix[i][j] = featureArray[j];
                }
            }
        }

        printMatrix(tempMatrix, matrixSize);


        double featureValue = 0;


        for (int i = 0; i < matrixSize; i++) {
            for (int j = i; j < matrixSize; j++) {
                if (j == i) {
                    featureValue = tempMatrix[i][j];
                }
                if (featureValue != 0) {
                    tempMatrix[i][j] /= featureValue;
                }

            }
        }


        printMatrix(tempMatrix, matrixSize);

        return tempMatrix;
    }


    void printMatrix(double[][] matrix, int matrixSize) {

        System.out.print("Print matrix: \n\n");

        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize; j++) {

                System.out.print(matrix[i][j] + "   ");

            }

            System.out.println("");
        }

    }

    public String serialize() {
        Gson gson = new Gson();
        return gson.toJson(featureMatrix.getData());
    }


    public static Feature deserialize(String jsonString) {
        Gson gson = new Gson();
        FeatureSerializer featureSerializer = gson.fromJson(jsonString, FeatureSerializer.class);
        return new Feature(featureSerializer.featureMatrix, featureSerializer.stdDevMatrix);

    }

    public void saveToFile(String fileName) throws IOException {

        FileUtils.write(new File(fileName), new FeatureSerializer(featureMatrix, stdDevMatrix).serialize(), "UTF-8");
    }

    public static Feature readFromFile(String fileName) throws IOException {
        String readString = FileUtils.readFileToString(new File(fileName), "UTF-8");
        return deserialize(readString);
    }

    class FeatureSerializer {

        double[][] featureMatrix;
        double[][] stdDevMatrix;

        public FeatureSerializer(RealMatrix featureMatrix, RealMatrix stdDevMatrix) {
            this.featureMatrix = featureMatrix.getData();
            this.stdDevMatrix = stdDevMatrix.getData();
        }

        public String serialize() {
            Gson gson = new Gson();
            return gson.toJson(this);
        }

    }

}
