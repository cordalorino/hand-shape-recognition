package imageprocessing;

import feature.FingerFeature;
import main.Controller;
import org.apache.commons.math3.linear.*;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static org.opencv.core.CvType.CV_8UC1;

public class FeatureExtractor {


    private RealVector coefficientsLine0_1;
    private RealVector coefficientsLine1_2;

    enum Fingers {
        PINKY, RING, MIDDLE, INDEX, THUMB;
    }

    FingerFeature[] fingerFeatures = null;

    boolean featuresValid = false;


    public FingerFeature[] calcHandFeatures(Mat roiFrame, List<MatOfPoint> contours, ArrayList<Point> handTips, ArrayList<Point> handDefects) {
        featuresValid = false;
        if (handDefects.size() < 4)
            return null;
        FingerFeature [] tempFingerFeatures = new FingerFeature[4];
        Point point0 = handDefects.get(0);
        Point point1 = handDefects.get(1);
        Point point2 = handDefects.get(2);
        Point tipPinkyFinger = handTips.get(0);
        Point tipRingFinger = handTips.get(1);
        Point tipMiddleFinger = handTips.get(2);
        Point tipIndexFinger = handTips.get(3);
        try {
            coefficientsLine0_1 = calcLineCoefficients(point0, point1);
            coefficientsLine1_2 = calcLineCoefficients(point1, point2);

            tempFingerFeatures[Fingers.RING.ordinal()] = extractFingerFeatures(roiFrame, contours, point0, point1, tipRingFinger, coefficientsLine0_1);
            tempFingerFeatures[Fingers.MIDDLE.ordinal()] = extractFingerFeatures(roiFrame, contours, point1, point2, tipMiddleFinger, coefficientsLine1_2);


            Point startingPointPinky = findPointAtDistance(coefficientsLine0_1, point0, (calcDistanceBetweenPoints(point0, point1) / 2));
            Point pinkyRightBasepoint = calcClosestIntersection(roiFrame, startingPointPinky, coefficientsLine0_1, contours, 3, true, true, (int) (calcDistanceBetweenPoints(point0, point1) / 2));

            Point startingPointIndex = findPointAtDistance(coefficientsLine1_2, point2, -(calcDistanceBetweenPoints(point1, point2) / 2));
            Point indexLeftBasepoint = calcClosestIntersection(roiFrame, startingPointIndex, coefficientsLine1_2, contours, 3, false, true, (int) (calcDistanceBetweenPoints(point1, point2) / 2));

            tempFingerFeatures[Fingers.PINKY.ordinal()] = extractFingerFeatures(roiFrame, contours, point0, pinkyRightBasepoint, tipPinkyFinger, coefficientsLine0_1);
            tempFingerFeatures[Fingers.INDEX.ordinal()] = extractFingerFeatures(roiFrame, contours, indexLeftBasepoint, point2, tipIndexFinger, coefficientsLine1_2);
            featuresValid = true;
            fingerFeatures = tempFingerFeatures;
        }catch (org.apache.commons.math3.linear.SingularMatrixException e){
            return null;
        }
        return fingerFeatures;
    }


    private RealVector calcLineCoefficients(Point defect1, Point defect2) {
        RealMatrix coefficients = new Array2DRowRealMatrix(new double[][]{{defect1.x, 1}, {defect2.x, 1}}, false);
        DecompositionSolver solver = new LUDecomposition(coefficients).getSolver();

        return solver.solve(new ArrayRealVector(new double[]{defect1.y, defect2.y}, false));
    }

    private FingerFeature extractFingerFeatures(Mat roiFrame, List<MatOfPoint> contours, Point leftDefect, Point rightDefect, Point fingerTip, RealVector baseLine) {


        Point middleFingerBase = calcMiddlePoint(leftDefect, rightDefect);

        Point middlePointFinger = calcMiddlePoint(middleFingerBase, fingerTip);


        RealVector coefficientsVerticalLine = calcLineCoefficients(middleFingerBase, fingerTip);
        double m = -1 / coefficientsVerticalLine.getEntry(0);
//        double m = baseLine.getEntry(0);
        double coefficientLineMiddle = middlePointFinger.y - m * middlePointFinger.x;

        RealVector coefficientLineMiddleRing = coefficientsVerticalLine.copy();
        coefficientLineMiddleRing.setEntry(1, coefficientLineMiddle);
        coefficientLineMiddleRing.setEntry(0, m);

        Point rightIntersection = calcClosestIntersection(roiFrame, middlePointFinger, coefficientLineMiddleRing, contours, 3, true, false, 999);
        Point leftIntersection = calcClosestIntersection(roiFrame, middlePointFinger, coefficientLineMiddleRing, contours, 3, false, false, 999);
        FingerFeature fingerFeature = new FingerFeature();
        fingerFeature.setFingerMiddleWidth(calcDistanceBetweenPoints(rightIntersection, leftIntersection));
        fingerFeature.setFingerBottomWidth(calcDistanceBetweenPoints(rightDefect, leftDefect));
        fingerFeature.setFingerHeight(calcDistanceBetweenPoints(fingerTip, middleFingerBase));

        return fingerFeature;

    }

    private Point findPointAtDistance(RealVector lineCoefficients, Point center, double distance) {
        double m = lineCoefficients.getEntry(0);
        double q = lineCoefficients.getEntry(1);

        double res = center.x;

        res += distance * Math.sqrt(1 + Math.pow(m, 2));

        return new Point(res, m * res + q);
    }

    private double calcDistanceBetweenPoints(Point point1, Point point2) {
        return Math.sqrt((Math.pow(point1.x - point2.x, 2) + Math.pow(point1.y - point2.y, 2)));
    }

    private Point calcMiddlePoint(Point point1, Point point2) {
        return new Point((point1.x + point2.x) / 2, (point1.y + point2.y) / 2);
    }


    private boolean kernelIntersectsContour(int size, Mat contourFrame, Point point) {
        size = (int) Math.ceil(size / 2);
        Rect roi = new Rect((int) point.x - size, (int) point.y - size, size * 2, size * 2);
        Mat submat = contourFrame.submat(roi);
        for (int i = 0; i < submat.rows(); i++) {
            for (int j = 0; j < submat.cols(); j++) {
                double[] valueAtPoint = submat.get(i, j);
                if (valueAtPoint[0] != 0) {
                    return true;
                }
            }
        }
        return false;
    }


    private Point calcClosestIntersection(Mat roiFrame, Point point, RealVector lineCoefficients, List<MatOfPoint> contours, int kernelSize, boolean right, boolean baseline, int maxDistance) {


        Mat zeroesFrame = Mat.zeros(roiFrame.size(), CV_8UC1);
        Imgproc.drawContours(zeroesFrame, contours, 0, new Scalar(255, 255, 255), 5);

//        Mat zeroesFrame2 = Mat.zeros(roiFrame.size(), CV_8UC1);
//        Core.flip(zeroesFrame, zeroesFrame2, 90);
//
//        Controller.instance.imgHand2.setImage(Utils.mat2Image(zeroesFrame2));


        double updatedX = point.x;
        double y = 0;
        Point intersectionPoint;
        if (right)
            maxDistance = (int) updatedX + maxDistance;
        else
            maxDistance = (int) updatedX - maxDistance;


        do {
            if (right)
                updatedX += 1;
            else
                updatedX -= 1;
            y = lineCoefficients.getEntry(0) * (updatedX) + lineCoefficients.getEntry(1);

            intersectionPoint = new Point((int) Math.floor(updatedX), (int) Math.floor(y));
            if (kernelIntersectsContour(kernelSize, zeroesFrame, intersectionPoint)) {
                if (true) {
                    break;
                }
            }
        } while ((!right && updatedX >= maxDistance) || (right && updatedX <= maxDistance));
        if (!baseline) {
            Scalar color;
            if (right)
                color = new Scalar(0, 0, 0);
            else
                color = new Scalar(255, 255, 255);
            Imgproc.circle(roiFrame, intersectionPoint, 5, color, 3);
        } else {
            Scalar color;
            color = new Scalar(0, 255, 0);
            Imgproc.circle(roiFrame, intersectionPoint, 5, color, 3);
        }
        return intersectionPoint;
    }


    public FingerFeature[] getFingerFeatures() {
        return fingerFeatures;
    }

    public boolean isFeaturesValid() {
        return featuresValid;
    }

}
