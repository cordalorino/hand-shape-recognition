package imageprocessing;

import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import utils.MathUtils;
import utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static org.opencv.core.Core.FONT_HERSHEY_SIMPLEX;

public class ImageProcessingHelper {
    private static Scalar mBlobColorRgba;
    public static FeatureExtractor featureExtractor;

    public static Mat processImage(Mat frame, Rect roi) {

        Mat frameROI = Utils.selectRoiOfImage(frame, roi);
        ColorBlobDetector mDetector = new ColorBlobDetector();
        Mat mSpectrum = new Mat();
        setUpDetector(frameROI, mDetector, mSpectrum);
        mDetector.process(frameROI);
        List<MatOfPoint> contours = mDetector.getContours();
        Imgproc.drawContours(frameROI, contours, 0, new Scalar(0, 255, 0));

        MatOfInt hull = new MatOfInt();
        Imgproc.convexHull(contours.get(0), hull);
        MatOfInt4 defects = new MatOfInt4();

        Imgproc.convexityDefects(contours.get(0), hull, defects);
        ArrayList<Point> handTips = new ArrayList<>();
        ArrayList<Point> handDefects = new ArrayList<>();

        calcTipsAndDefects(frameROI, contours, defects, handTips, handDefects);

        drawTipsAndDefects(frameROI, handTips, handDefects);

        featureExtractor = new FeatureExtractor();
        featureExtractor.calcHandFeatures(frameROI, contours, handTips, handDefects);

        drawColorRectangle(frame);

        Mat spectrumLabel = frame.submat(0, mSpectrum.rows(), 0, mSpectrum.cols());
        mSpectrum.copyTo(spectrumLabel);

        drawRoiCenter(frameROI);

        Mat roiLabel = frame.submat(roi);

        frameROI.copyTo(roiLabel);

        drawRoiRectangle(frame, roi.tl(), roi.br(), 5);

        return frame;
    }









    private static void drawRoiRectangle(Mat frame, Point tl, Point br, int thickness) {
        Imgproc.rectangle(frame, tl, br, new Scalar(0, 0, 255), thickness);
    }

    private static void drawRoiCenter(Mat frameROI) {
        Imgproc.rectangle(frameROI, new Point(frameROI.width() / 2 - 3, frameROI.height() / 2 - 3), new Point(frameROI.width() / 2 + 3, frameROI.height() / 2 + 3), new Scalar(0, 0, 255), 10);
    }

    private static void drawColorRectangle(Mat frame) {
        Mat colorLabel = frame.submat(4, 68, 4, 68);
        colorLabel.setTo(mBlobColorRgba);
    }

    private static void calcTipsAndDefects(Mat frameROI, List<MatOfPoint> contours, MatOfInt4 defects, ArrayList<Point> handTips, ArrayList<Point> handDefects) {
        Point data[] = contours.get(0).toArray();
        List<Integer> cdList = defects.toList();
        Rect boundingBox = Imgproc.boundingRect(contours.get(0));
        drawBoundingBox(frameROI, boundingBox);
        Point center = new Point(boundingBox.x + boundingBox.width / 2, boundingBox.y + boundingBox.height / 2);

        findHandTipsAndDefects(data, cdList, boundingBox, center, handTips, handDefects);
    }

    private static void drawBoundingBox(Mat frameROI, Rect boundingBox) {
        Imgproc.rectangle(frameROI, boundingBox.tl(), boundingBox.br(), new Scalar(255, 0, 0));
    }

    private static void drawTipsAndDefects(Mat frameROI, ArrayList<Point> handTips, ArrayList<Point> handDefects) {
        for (int i = 0; i < handTips.size(); i++) {
            Imgproc.circle(frameROI, handTips.get(i), 9, new Scalar(0, 255, 0), 2);
            Imgproc.putText(frameROI, String.valueOf(i), handTips.get(i), 2, 1, new Scalar(0, 255, 0));
            Imgproc.circle(frameROI, handDefects.get(i), 9, new Scalar(0, 0, 255), 2);
            Imgproc.putText(frameROI, String.valueOf(i), handDefects.get(i), FONT_HERSHEY_SIMPLEX, 1, new Scalar(0, 0, 255));

        }
    }

    private static void findHandTipsAndDefects(Point[] data, List<Integer> cdList, Rect boundingBox, Point center, ArrayList<Point> handTips, ArrayList<Point> handDefects) {
        for (int j = 0; j < cdList.size(); j = j + 4) {
            Point start = data[cdList.get(j)];
            Point end = data[cdList.get(j + 1)];
            Point defect = data[cdList.get(j + 2)];
            //Point depth = data[cdList.get(j+3)];
            double angle = Math.atan2(center.y - start.y, center.x - start.x) * 180 / Math.PI;
            double inAngle = MathUtils.innerAngle(start.x, start.y, end.x, end.y, defect.x, defect.y);

            double length = Math.sqrt(Math.pow(start.x - end.x, 2) + Math.pow(start.y - end.y, 2));
            if (angle > -30 && angle < 160 && Math.abs(inAngle) > 20 && Math.abs(inAngle) < 120 && length > 0.1 * boundingBox.height) {
                handTips.add(start);
                handDefects.add(defect);

            }
        }
    }

    private static void setUpDetector(Mat mRgba, ColorBlobDetector mDetector, Mat mSpectrum) {
        int cols = mRgba.cols();
        int rows = mRgba.rows();
        mBlobColorRgba = new Scalar(255);
        Scalar mBlobColorHsv = new Scalar(255);
        Size SPECTRUM_SIZE = new Size(200, 64);
        Scalar CONTOUR_COLOR = new Scalar(255, 0, 0, 255);

        int x = mRgba.width() / 2;
        int y = mRgba.height() / 2;
        ;


        if ((x < 0) || (y < 0) || (x > cols) || (y > rows))
            return;

        Rect touchedRect = new Rect();

        touchedRect.x = (x > 4) ? x - 4 : 0;
        touchedRect.y = (y > 4) ? y - 4 : 0;

        touchedRect.width = (x + 4 < cols) ? x + 4 - touchedRect.x : cols - touchedRect.x;
        touchedRect.height = (y + 4 < rows) ? y + 4 - touchedRect.y : rows - touchedRect.y;

        Mat touchedRegionRgba = mRgba.submat(touchedRect);
        Mat touchedRegionHsv = new Mat();
        Imgproc.cvtColor(touchedRegionRgba, touchedRegionHsv, Imgproc.COLOR_RGB2HSV_FULL);

        // Calculate average color of touched region
        mBlobColorHsv = Core.sumElems(touchedRegionHsv);
        int pointCount = touchedRect.width * touchedRect.height;
        for (int i = 0; i < mBlobColorHsv.val.length; i++)
            mBlobColorHsv.val[i] /= pointCount;

        mBlobColorRgba = converScalarHsv2Rgba(mBlobColorHsv);


        mDetector.setHsvColor(mBlobColorHsv);

        Imgproc.resize(mDetector.getSpectrum(), mSpectrum, SPECTRUM_SIZE);


        touchedRegionRgba.release();
        touchedRegionHsv.release();
    }

    private static Scalar converScalarHsv2Rgba(Scalar hsvColor) {
        Mat pointMatRgba = new Mat();
        Mat pointMatHsv = new Mat(1, 1, CvType.CV_8UC3, hsvColor);
        Imgproc.cvtColor(pointMatHsv, pointMatRgba, Imgproc.COLOR_HSV2RGB_FULL, 4);

        return new Scalar(pointMatRgba.get(0, 0));
    }

}
